# CANDR Data Exploration Streamlit

This is a streamlit project, managed using poetry

```bash
poetry install
poetry run streamlit run app.py
```
