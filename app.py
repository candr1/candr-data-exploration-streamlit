import streamlit as st
from streamlit_sortables import sort_items
import pandas as pd
import plotly.express as px

st.set_page_config(layout='wide')

st.write('# Exploring the CANDR analysis results')

df = pd.read_parquet('items_pca2and3_withitems.parquet')

df['str_ID'] = df['ID'].astype("string")
df['marker_size'] = 1

item_columns = [c for c in df.columns if c.startswith('MEI')]
min_n = min(df['n'])
max_n = max(df['n'])
all_ids = sorted(list(df['ID'].unique()))

colour_by = st.selectbox('Colour by', ('ID', 'Items', 'n'), 1)
items_container = st.container()
select_all_items = st.checkbox("Select all", False)
if select_all_items:
    items = items_container.multiselect('Items', item_columns, item_columns)
else:
    items = items_container.multiselect('Items', item_columns, [item_columns[0]])
id_container = st.container()
select_all_ids = st.checkbox("Select all", True)
if select_all_ids:
    id = id_container.multiselect('ID', all_ids, all_ids)
else:
    id = id_container.multiselect('ID', all_ids, [])

n = st.slider('N', min_n, max_n, (min_n, max_n), step=1)
size_to_n = st.toggle('Scale size by N', False)
if size_to_n:
    marker_size = 'n'
else:
    marker_size = 'marker_size'

condition = df[items].any(axis=1)
filtered_items = df[condition]

filtered_n = filtered_items[(filtered_items['n'] >= n[0]) & (filtered_items['n'] <= n[1])]

filtered_ids = filtered_n[filtered_n['ID'].isin(id)]

filtered = filtered_ids

category_orders = dict()

if colour_by == 'Items':
    filtered['colour'] = "Not found"
    def colour_items(row):
        items_cols = row[item_columns]
        items_in_row = list(items_cols[items_cols == True].index)
        if len(items_in_row) == 0:
            return row
        if len(items_in_row) == 1:
            item = items_in_row[0]
            row['colour'] = item
            return row
        matches = tuple([item for item in items if item in items_in_row])
        if len(matches) == 1:
            row['colour'] = str(matches[0])
        elif len(matches) >= 2:
            row['colour'] = str(matches)
        return row
    filtered = filtered.apply(colour_items, axis=1)
    colour_by = 'colour'
elif colour_by == 'ID':
    categorical_ID = st.toggle('Colour IDs categorically', False)
    if categorical_ID:
        colour_by = 'str_ID'
        ids = list(filtered['str_ID'].unique())
        sorted_ids = sort_items(ids)
        category_orders['str_ID'] = sorted_ids
three_d = st.toggle('3D', True)
showlegend = st.toggle('Legend', False)
if three_d:
    fig = px.scatter_3d(filtered, x='PCA3a 1', y='PCA3a 2', z='PCA3a 3', color=colour_by, hover_data=['Items', 'n', 'ID'], size=marker_size, category_orders=category_orders)
else:
    fig = px.scatter(filtered, x='PCA3a 1', y='PCA3a 2', color=colour_by, hover_data=['Items', 'n', 'ID'], size=marker_size, category_orders=category_orders)
if colour_by =='colour':
    fig.update_layout(coloraxis_showscale=False)
fig.update_layout(showlegend=showlegend)

st.plotly_chart(fig, use_container_width=True, height=800)
